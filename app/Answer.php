<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
	protected $fillable = [
		'text',
		'persian_text',
    'is_reviewed'
	];

	public function question(){
		return $this->belongsTo('App\Question');
	}
}
