<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'persian_name'
    ];

    public function categories(){
        return $this->belongsToMany('App\Question');
    }

    public function questions()
    {
        return $this->hasMany('App\Question');
    }
}
