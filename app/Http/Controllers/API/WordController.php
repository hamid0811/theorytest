<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Word;
use Illuminate\Http\Request;

class WordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Word::whereNull('is_translation_successful')->limit(10)->get();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $words = json_decode($request->getContent(), true);
        foreach ($words as $key => $word) {
            if ($word['persian_translation'] !== "" && $word['english'] !== "") {
                Word::where('text', $word['english'])
                    ->update([
                        'persian_translation' => $word['persian_translation'],
                        'additional_translations' => $word['additional_translations'],
                        'is_translation_successful' => true
                    ]);
            } else {
                Word::where('text', $word['english'])->update(['is_translation_successful' => false]);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function show($word)
    {
        $tranlation = Word::where('text', $word)->where('is_translation_successful', true)->first();
        return $tranlation;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Word $word)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function destroy(Word $word)
    {
        //
    }
}
