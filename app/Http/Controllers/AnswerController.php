<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answers = Answer::with('question')->Paginate(10);
        return view('answer.index', compact('answers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function show(Answer $answer)
    {
        $relatedAnswers = Answer::where('question_id', $answer->question->id)->where('id', '!=', $answer->id)->get();
        return view('answer.show', compact('answer', 'relatedAnswers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Answer $answer)
    {
        $relatedAnswers = Answer::where('question_id', $answer->question->id)->where('id', '!=', $answer->id)->get();
        return view('answer.edit', compact('answer', 'relatedAnswers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Answer $answer)
    {
        $request->validate([
            'text' => 'bail|required|max:255',
            'persian_text' => 'bail|required|max:255'
        ],
        [
            'text.required' => 'English text field cannot be empty',
            'persian_text.required' => 'Persian text field cannot be empty'
        ]);

        $answer->update($request->all());

        return redirect()->route('answer.show', $answer)->with('message', 'Answer updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Answer $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Answer $answer)
    {
        $question->delete();
        return redirect()->route('answer.index')->with('message', 'Answer deleted');
    }
}
