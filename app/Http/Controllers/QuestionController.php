<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $questions = Question::with('category')->Paginate(10);
        return view('question.index', compact('questions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'questions' => 'bail|required',
            'category' => 'bail|required',
        ],
        [
            'questions.required' => 'A list of questions is required',
            'category.required' => 'A category is required'
        ]);

        $questions = json_decode($request->file('questions')->get());

        if (!isset($questions[0]->question)) {
            foreach ($questions as $key => $question) {
                $questionModel = new Question;
                $questionModel->category_id = $request->category;
                $questionModel->question_number = $key + 1;

                if (isset($question->image)) {
                    $url = $question->image_src;
                    $info = pathinfo($url);
                    Storage::makeDirectory('public/images');

                    $imageName = $info['filename'];

                    $image_resize = Image::make(file_get_contents($url));
                    $image_resize->save(public_path('storage/images/'.$imageName.'.jpg'));
                    $questionModel->image = $imageName.'.jpg';
                    $questionModel->image_src = $url;
                }

                $questionModel->text = $question->text;
                $questionModel->correct_answer = $question->correct_answer;
                $questionModel->incorrect_text = $question->incorrect_text;
                $questionModel->persian_text = $question->persian_text;
                $questionModel->persian_incorrect_text = $question->persian_incorrect_text;
                $questionModel->is_reviewed = $question->is_reviewed;

                $questionModel->save();

                $answers = $question->answers;
                foreach ($answers as $key => $answer) {
                    $answerModel = new Answer;
                    $answerModel->question_id = $questionModel->id;
                    $answerModel->text = $answer->text;
                    $answerModel->persian_text = $answer->persian_text;
                    $answerModel->answer_number = $answer->answer_number;
                    $answerModel->is_answer = (int) $answer->is_answer;
                    $answerModel->is_reviewed = (int) $answer->is_reviewed;
                    $answerModel->save();
                }
            }
        } else {
            foreach ($questions as $key => $question) {
                $questionModel = new Question;
                $questionModel->category_id = $request->category;
                $questionModel->question_number = $key + 1;

                if (isset($question->image)) {
                    $url = $question->image;
                    $info = pathinfo($url);
                    Storage::makeDirectory('public/images');

                    $imageName = $info['filename'];

                    $image_resize = Image::make(file_get_contents($url));
                    $image_resize->save(public_path('storage/images/'.$imageName.'.jpg'));
                    $questionModel->image = $imageName.'.jpg';
                    $questionModel->image_src = $url;
                }

                $questionModel->text = $question->question;
                $questionModel->correct_answer = $question->correctAnswer;
                $questionModel->incorrect_text = $question->incorrectText;
                $questionModel->persian_text = $question->persianText;

                if (isset($question->persianIncorrectText)) {
                    $questionModel->persian_incorrect_text = $question->persianIncorrectText;
                }

                $questionModel->save();

                $answers = $question->answers;
                foreach ($answers as $key => $answer) {
                    $answerModel = new Answer;
                    $answerModel->question_id = $questionModel->id;
                    $answerModel->text = $answer->answer;
                    $answerModel->persian_text = $answer->persianText;
                    $answerModel->answer_number = $answer->answerNumber;
                    $answerModel->is_answer = (int) $answer->isCorrect;
                    $answerModel->save();
                }
            }
        }

        return redirect()->route('home')->with('message', 'Questions uploaded');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show(Question $question)
    {
        return view('question.show', compact('question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        return view('question.edit', compact('question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question)
    {
        $request->validate([
            'text' => 'bail|required|max:255',
            'persian_text' => 'bail|required|max:255'
        ]);

        $question->update($request->all());

        return redirect()->route('question.show', $question)->with('message', 'Question updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        $question->delete();
        return redirect()->route('question.index')->with('message', 'Question deleted');
    }
}
