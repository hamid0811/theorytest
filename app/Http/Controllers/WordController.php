<?php

namespace App\Http\Controllers;

use App\Question;
use App\Word;
use Illuminate\Http\Request;

class WordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function show(Word $word)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function edit(Word $word)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Word $word)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Word  $word
     * @return \Illuminate\Http\Response
     */
    public function destroy(Word $word)
    {
        //
    }

    public function extractor() {
        $questions = Question::where('is_translated', false)->limit(50)->with('answers')->get();
        var_dump($questions);
        $allQuestionsWords = [];

        foreach ($questions as $key => $question) {
            $allQuestionsWords[] = $this->extractWordsFromText($question->text);
            $allQuestionsWords[] = $this->extractWordsFromText($question->incorrect_text);

            foreach ($question->answers as $answer) {
                $allQuestionsWords[] = $this->extractWordsFromText($answer->text);
                $answer->is_translated = true;
                $answer->save();
            }

            $question->is_translated = true;
            $question->save();
        }

        $words = call_user_func_array('array_merge', $allQuestionsWords);

        foreach ($words as $word) {
            if ($word !== "") {
                Word::firstOrCreate(['text' => $word],['text' => $word]);
            }
        }
    }

    /**
     * Extacts a words from a given text
     *
     * @param  $text
     * @return array
     */
    public function extractWordsFromText($text) {
        $textWords = [];
        $text = preg_replace('/[^A-Za-z0-9\-]/', ' ', $text);
        $words = explode(' ', $text);
        foreach ($words as $key => $word) {
            if ($word !== "") {
                $textWords[] = $word;
            }
        }
        return $textWords;
    }
}
