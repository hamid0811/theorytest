<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'text',
        'persian_text',
        'incorrect_text',
        'persian_incorrect_text',
        'is_reviewed'
    ];

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function answers(){
        return $this->hasMany('App\Answer');
    }
}
