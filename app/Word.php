<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Word extends Model
{
     protected $casts = [
        'additional_translations' => 'array'
    ];

    protected $fillable = [
        'text',
        'persian_translation',
        'additional_translations'
    ];
}
