<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id');
            $table->foreignId('test_id')->nullable();
            $table->integer('question_number');
            $table->string('image')->nullable();
            $table->string('image_src')->nullable();
            $table->string('text');
            $table->integer('correct_answer');
            $table->mediumText('incorrect_text')->nullable();
            $table->string('persian_text');
            $table->mediumText('persian_incorrect_text')->nullable();
            $table->boolean('is_reviewed')->default(false);
            $table->boolean('is_translated')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
