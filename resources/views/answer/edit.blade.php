@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('answer.update', $answer) }}" method="post">
                        @csrf
                        @method('patch')
                        <table class="table table-borderless">
                            <tbody>
                                <tr >
                                    <td class="font-weight-bold">ID:</td>
                                    <td>{{ $answer->id }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Answer number:</td>
                                    <td>{{ $answer->answer_number }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Is reviewed:</td>
                                    <td>
                                        <switch-component :disabled="false" :state="{{ $answer->is_reviewed }}"></switch-component>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Correct answer:</td>
                                    <td>{{ $answer->is_answer }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">English text:</td>
                                    <td>
                                        <input class="form-control" type="text" name="text" value="{{ $answer->text }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Persian text:</td>
                                    <td>
                                        <input class="form-control" dir="rtl" type="text" name="persian_text" value="{{ $answer->persian_text }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold text-nowrap">Related answers:</td>
                                    <td>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th class="text-uppercase">English Text</th>
                                                    <th class="text-right text-uppercase">Persian Text</th>
                                                    <th class="text-right text-uppercase">Is Correct</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($relatedAnswers as $answer)
                                                    <tr>
                                                        <td>{{ $answer->answer_number }}</td>
                                                        <td>{{ $answer->text }}</td>
                                                        <td class="text-right">{{ $answer->persian_text }}</td>
                                                        <td class="text-right">
                                                            @if($answer->is_answer)
                                                                <i style="color: green" class="fas fa-check"></i>
                                                            @endif
                                                        </td>
                                                        <td class="text-right">
                                                            <a href="{{ route('answer.edit', $answer) }}" type="button" class="btn btn-secondary">Edit</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold text-nowrap">Answer's question:</td>
                                    <td>
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr >
                                                    <td>ID:</td>
                                                    <td>{{ $answer->question->id }}</td>
                                                </tr>
                                                <tr>
                                                    <td>English text:</td>
                                                    <td>{{ $answer->question->text }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Persian text:</td>
                                                    <td>{{ $answer->question->persian_text }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Category:</td>
                                                    <td>{{ $answer->question->category->name }}</td>
                                                </tr>
                                                <tr>
                                                    <td>Incorrect text:</td>
                                                    <td>{{ $answer->question->incorrect_text }}</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-nowrap">Persian incorrect text</td>
                                                    <td>{{ $answer->question->persian_incorrect_text }}</td>
                                                </tr>
                                                <tr>
                                                    <td ></td>
                                                    <td class="text-right">
                                                        <a href="{{ route('question.edit', $answer->question) }}" type="button" class="btn btn-secondary">Edit</a>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-secondary">Update</button>
                        <form action="{{ route('answer.delete', $answer) }}" method="post">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
