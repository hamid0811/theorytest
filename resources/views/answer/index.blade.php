@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Answers</div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Answer</th>
                                <th class="text-right">Persian Text</th>
                                <th class="text-right">Reviewed</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($answers as $answer)

                                <tr>
                                    <td>{{ $answer->id }}</td>
                                    <td>{{ $answer->text }}</td>
                                    <td class="text-right">{{ $answer->persian_text }}</td>
                                    <td class="text-center">
                                        @if($answer->is_reviewed)
                                            <i style="color: green" class="fas fa-check"></i>
                                        @else
                                            <i style="color: red" class="fas fa-times"></i>
                                        @endif
                                    </td>
                                    <td class="text-right">
                                        <a type="button" href="{{ route('answer.show', $answer) }}" style="color: white;" class="btn btn-secondary">View</a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    {{ $answers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
