@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr >
                                <td class="font-weight-bold">ID:</td>
                                <td>{{ $answer->id }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Answer #:</td>
                                <td>{{ $answer->answer_number }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Correct answer:</td>
                                <td>
                                    @if($answer->is_answer)
                                        <i style="color: green" class="fas fa-check"></i>
                                    @else
                                        <i style="color: red" class="fas fa-times"></i>
                                    @endif
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Is reviewed:</td>
                                <td>
                                    <switch-component :disabled="true" :state="{{ $answer->is_reviewed }}"></switch-component>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">English text:</td>
                                <td>{{ $answer->text }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Persian text:</td>
                                <td>{{ $answer->persian_text }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">Related answers:</td>
                                <td>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-nowrap text-uppercase">English Text</th>
                                                <th class="text-nowrap text-right text-uppercase">Persian Text</th>
                                                <th class="text-nowrap text-center text-uppercase">Is Correct</th>
                                                <th class="text-nowrap text-center text-uppercase">Is reviewed</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($relatedAnswers as $relatedAnswer)
                                                <tr>
                                                    <td>{{ $relatedAnswer->answer_number }}</td>
                                                    <td>{{ $relatedAnswer->text }}</td>
                                                    <td class="text-right">{{ $relatedAnswer->persian_text }}</td>
                                                    <td class="text-center">
                                                        @if($relatedAnswer->is_answer)
                                                            <i style="color: green" class="fas fa-check"></i>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if($relatedAnswer->is_reviewed)
                                                            <i style="color: green" class="fas fa-check"></i>
                                                        @else
                                                            <i style="color: red" class="fas fa-times"></i>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="{{ route('answer.show', $relatedAnswer) }}" type="button" class="btn btn-primary">View</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">Answer's question:</td>
                                <td>
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr >
                                                <td>ID:</td>
                                                <td>{{ $answer->question->id }}</td>
                                            </tr>
                                            <tr>
                                                <td>English text:</td>
                                                <td>{{ $answer->question->text }}</td>
                                            </tr>
                                            <tr>
                                                <td>Persian text:</td>
                                                <td>{{ $answer->question->persian_text }}</td>
                                            </tr>
                                            <tr>
                                                <td>Category:</td>
                                                <td>{{ $answer->question->category->name }}</td>
                                            </tr>
                                            <tr>
                                                <td>Incorrect text:</td>
                                                <td>{{ $answer->question->incorrect_text }}</td>
                                            </tr>
                                            <tr>
                                                <td class="text-nowrap">Persian incorrect text</td>
                                                <td>{{ $answer->question->persian_incorrect_text }}</td>
                                            </tr>
                                            <tr>
                                                <td ></td>
                                                <td class="text-right">
                                                    <a href="{{ route('question.show', $answer->question) }}" type="button" class="btn btn-primary">View</a>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <form action="{{ route('answer.delete', $answer) }}" method="post">
                    <a href="{{ route('answer.edit', $answer) }}" type="button" class="btn btn-secondary">Edit</a>
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
