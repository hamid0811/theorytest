@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="card mt-4">
                <div class="card-header">Edit category</div>
                <div class="card-body">
                    <form action="{{ route('category.update', $category) }}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="input-group mb-3">
                            <input type="text" class="form-control" name="name" value="{{ $category->name }}">
                        </div>
                        <div class="input-group mb-3">
                            <input type="text" dir="rtl" class="form-control" name="persian_name" value="{{ $category->persian_name }}">
                        </div>
                        <button class="btn btn-primary" type="submit">Update</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
