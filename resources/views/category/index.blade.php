@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Categories</div>
                <div class="card-body">
                    <ul class="list-group mb-3">
                        @foreach ($categories as $category)
                            <li class="list-group-item d-flex justify-content-between align-items-center">
                                {{ $category->name }}
                                <span class="badge badge-primary badge-pill">{{ count($category->questions) }} {{"Questions"}}</span>
                                {{ $category->persian_name }}
                                <div class="btn-group">
                                    <a href="{{ route('category.show', $category) }}" type="button" class="btn btn-secondary">View</a>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                    <a href="{{ route('category.create') }}" type="button" class="btn btn-primary">New category</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
