@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center mb-4">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="list-group mb-3">
                        <li class="list-group-item d-flex justify-content-between align-items-center">
                            {{ $category->name }}
                            <span>{{ $category->persian_name }}</span>
                            <div class="btn-group">
                                    <a href="{{ route('category.edit', $category) }}" type="button" class="btn btn-secondary">Edit</a>
                                    <a href="{{ route('category.download', $category) }}" type="button" class="btn btn-secondary">Download</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header"><h4>{{ count($category->questions)}} Questions</h4></div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Question</th>
                                <th>Translated</th>
                                <th>Reviewed</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($category->questions as $question)
                                <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{{ $question->text }}</td>
                                    <td style="text-align: center">
                                        @if($question->persian_text)
                                            <i style="color: green" class="fas fa-check"></i>
                                        @else
                                            <i style="color: red" class="fas fa-times"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($question->is_reviewed)
                                            <i style="color: green" class="fas fa-check"></i>
                                        @else
                                            <i style="color: red" class="fas fa-times"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <a type="button" href="{{ route('question.show', $question) }}" style="color: white;" class="btn btn-secondary">View</a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
