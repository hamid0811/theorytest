@extends('layouts.app')

@section('content')
<div class="container main-container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Upload Questions</div>
                <div class="card-body">
                    <form action="{{ route('question.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <p>Select Qestions:</p>
                        <div class="custom-file mb-3">
                            <input type="file" class="custom-file-input" id="questions" name="questions">
                            <label class="custom-file-label" for="customFile">Choose file</label>
                        </div>
                        <div class="form-group">
                            <label for="categories">Select a category:</label>
                            <select class="form-control" id="categories" name="category">
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mt-3">
                          <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function($){
            $(".custom-file-input").on("change", function() {
              var fileName = $(this).val().split("\\").pop();
              $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });
        });
    </script>
@endpush
