@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('question.update', $question) }}" method="post">
                        @csrf
                        @method('patch')
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td class="font-weight-bold text-uppercase">ID</td>
                                    <td>{{ $question->id }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">English text:</td>
                                    <td>
                                        <input class="form-control" type="text" name="text" value="{{ $question->text }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Persian text:</td>
                                    <td>
                                        <input class="form-control" type="text" dir="rtl" name="persian_text" value="{{ $question->persian_text }}">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Category:</td>
                                    <td>{{ $question->category->name }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Correct answer:</td>
                                    <td>{{ $question->correct_answer }}</td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Is reviewed:</td>
                                    <td>
                                        <switch-component :disabled="false" :state="{{ $question->is_reviewed }}"></switch-component>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Incorrect text:</td>
                                    <td>
                                        <textarea class="form-control" rows="3" name="incorrect_text" id="incorrect_text">{{ $question->incorrect_text }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Persian incorrect text:</td>
                                    <td>
                                        <textarea
                                        	class="form-control"
                                        	rows="3"
                                        	dir="rtl"
                                        	name="persian_incorrect_text"
                                        	id="persian_incorrect_text">{{ $question->persian_incorrect_text }}</textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="font-weight-bold">Answers:</td>
                                    <td>
                                        <table class="table table-bordered table-striped">
	                                        <thead>
	                                            <tr>
	                                                <th>#</th>
	                                                <th class="text-uppercase">English text</th>
	                                                <th class="text-right text-uppercase">Persian text</th>
	                                                <th class="text-right text-uppercase">Is correct</th>
                                                    <th class="text-right text-uppercase">Is reviewed</th>
	                                                <th></th>
	                                            </tr>
	                                        </thead>
	                                        <tbody>
	                                            @foreach($question->answers as $answer)
	                                                <tr>
	                                                    <td>{{ $answer->answer_number }}</td>
	                                                    <td>{{ $answer->text }}</td>
	                                                    <td class="text-right">{{ $answer->persian_text }}</td>
	                                                    <td class="text-center">
	                                                        @if($answer->is_answer)
	                                                            <i style="color: green" class="fas fa-check"></i>
	                                                        @endif
	                                                    </td>
                                                        <td class="text-center">
                                                            @if($answer->is_reviewed)
                                                                <i style="color: green" class="fas fa-check"></i>
                                                            @else
                                                                <i style="color: red" class="fas fa-times"></i>
                                                            @endif
                                                        </td>
	                                                    <td class="text-right">
	                                                        <a href="{{ route('answer.edit', $answer) }}" type="button" class="btn btn-secondary">Edit</a>
	                                                    </td>
	                                                </tr>
	                                            @endforeach
	                                        </tbody>
	                                    </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <button type="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
