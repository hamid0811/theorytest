@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Questions</div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Question</th>
                                <th>Category</th>
                                <th>Translated</th>
                                <th>Reviewed</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($questions as $question)
                                <tr>
                                    <td>{{ $question->id }}</td>
                                    <td>{{ $question->text }}</td>
                                    <td>{{ $question->category->name }}</td>
                                    <td style="text-align: center">
                                        @if($question->persian_text)
                                            <i style="color: green" class="fas fa-check"></i>
                                        @else
                                            <i style="color: red" class="fas fa-times"></i>
                                        @endif
                                    </td>
                                    <td class="text-center">
                                        @if($question->is_reviewed)
                                            <i style="color: green" class="fas fa-check"></i>
                                        @else
                                            <i style="color: red" class="fas fa-times"></i>
                                        @endif
                                    </td>
                                    <td>
                                        <a type="button" href="{{ route('question.show', $question) }}" style="color: white;" class="btn btn-secondary">View</a>
                                    </td>
                                </tr>
                            @endforeach

                        </tbody>
                    </table>
                    {{ $questions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
