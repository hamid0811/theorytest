@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-borderless">
                        <tbody>
                            <tr >
                                <td class="font-weight-bold">ID</td>
                                <td>{{ $question->id }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">English text:</td>
                                <td>{{ $question->text }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Persian text:</td>
                                <td>{{ $question->persian_text }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Category:</td>
                                <td>{{ $question->category->name }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">Correct answer number:</td>
                                <td>{{ $question->correct_answer }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Is reviewed:</td>
                                <td>
                                    <switch-component :disabled="true" :state="{{ $question->is_reviewed }}"></switch-component>
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Incorrect text:</td>
                                <td>{{ $question->incorrect_text }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold text-nowrap">Persian incorrect text:</td>
                                <td>{{ $question->persian_incorrect_text }}</td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold">Answers:</td>
                                <td>
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th class="text-uppercase">English text:</th>
                                                <th class="text-right text-uppercase">Persian Text</th>
                                                <th class="text-center text-uppercase">Is Correct</th>
                                                <th class="text-center text-uppercase">Is reviewed</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($question->answers as $answer)
                                                <tr>
                                                    <td>{{ $answer->answer_number }}</td>
                                                    <td>{{ $answer->text }}</td>
                                                    <td class="text-right">{{ $answer->persian_text }}</td>
                                                    <td class="text-center">
                                                        @if($answer->is_answer)
                                                            <i style="color: green" class="fas fa-check"></i>
                                                        @endif
                                                    </td>
                                                    <td class="text-center">
                                                        @if($answer->is_reviewed)
                                                            <i style="color: green" class="fas fa-check"></i>
                                                        @else
                                                            <i style="color: red" class="fas fa-times"></i>
                                                        @endif
                                                    </td>
                                                    <td class="text-right">
                                                        <a href="{{ route('answer.show', $answer) }}" type="button" class="btn btn-primary">View</a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <form action="{{ route('question.delete', $question) }}" method="post">
                    <a href="{{ route('question.edit', $question) }}" type="button" class="btn btn-secondary">Edit</a>
                        @csrf
                        @method('delete')
                        <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
