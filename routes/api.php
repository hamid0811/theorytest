<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('questions/random/', 'API\QuestionController@random');
Route::get('words/', 'API\WordController@index');
Route::get('words/{word}', 'API\WordController@show');
Route::post('words/', 'API\WordController@store');

Route::apiResource('categories', 'API\CategoryController');
Route::apiResource('questions', 'API\QuestionController');
