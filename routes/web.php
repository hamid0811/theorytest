<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'questions', 'as'=>'question.'], function () {
  Route::get('/', 'QuestionController@index')->name('index');
  Route::get('/show/{question}', 'QuestionController@show')->name('show');
  Route::post('/store', 'QuestionController@store')->name('store');
  Route::get('edit/{question}', 'QuestionController@edit')->name('edit');
  Route::delete('delete/{question}','QuestionController@destroy')->name('delete');
  Route::get('edit/{question}', 'QuestionController@edit')->name('edit');
  Route::patch('update/{question}', 'QuestionController@update')->name('update');
});

Route::group(['prefix'=>'categories', 'as'=>'category.'], function () {
  Route::get('/', 'CategoryController@index')->name('index');
  Route::post('store', 'CategoryController@store')->name('store');
  Route::get('edit/{category}', 'CategoryController@edit')->name('edit');
  Route::patch('update/{category}', 'CategoryController@update')->name('update');
  Route::delete('delete/{category}','CategoryController@destroy')->name('delete');
  Route::get('create', 'CategoryController@create')->name('create');
  Route::get('show/{category}', 'CategoryController@show')->name('show');
  Route::get('download/{category}', 'CategoryController@download')->name('download');
});

Route::group(['prefix'=>'answers', 'as'=>'answer.'], function () {
  Route::get('/', 'AnswerController@index')->name('index');
  Route::get('show/{answer}', 'AnswerController@show')->name('show');
  Route::get('edit/{answer}', 'AnswerController@edit')->name('edit');
  Route::delete('delete/{answer}','AnswerController@destroy')->name('delete');
  Route::patch('update/{answer}', 'AnswerController@update')->name('update');
});

Route::group(['prefix'=>'words', 'as'=>'word.'], function () {
  Route::get('/show', 'WordController@show')->name('show');
  Route::get('/extractor', 'WordController@extractor')->name('extract');
});
